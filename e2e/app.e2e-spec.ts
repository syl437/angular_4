import { Angular44Page } from './app.po';

describe('angular44 App', () => {
  let page: Angular44Page;

  beforeEach(() => {
    page = new Angular44Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
